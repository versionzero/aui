// this is loaded first so we'll set up the global namespace here
define("MainModule", [], function(){
    var sandboxModule = angular.module('sandbox', []);
    return sandboxModule;
});