var http = require('http'),
	fs = require('fs'),
	request = require('request'),
	express = require('express');

var CONFIG = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8'));

var auifiddle_server = express();

auifiddle_server.use(express.bodyParser());

//GET

auifiddle_server.get("/static/js/SaveCtrl.js", function(req, res){
	fs.readFile(__dirname + "/sandbox_ui/static/js/SaveCtrl-Live.js", function(error, content) {
            res.writeHead(200, { 'Content-Type': "application/javascript" });
            res.end(content, 'utf-8');    
        });
})

auifiddle_server.get("/code/:hash-:rev", function(req, res){
	var hash = req.params.hash,
		revision = req.params.rev;
	getCodeRevision(hash, revision, res);
});

auifiddle_server.get("/code/:hash", function(req, res){
	var hash = req.params.hash;
	getCode(hash, null, res);
});

auifiddle_server.get("/:hash", function(req, res){
	fs.readFile(__dirname + "/sandbox_ui/index.html", function(error, content) {
            res.writeHead(200, { 'Content-Type': "text/html" });
            res.end(content, 'utf-8');    
        });
})

auifiddle_server.get("/:hash/:rev", function(req, res){
	fs.readFile(__dirname + "/sandbox_ui/index.html", function(error, content) {
            res.writeHead(200, { 'Content-Type': "text/html" });
            res.end(content, 'utf-8');    
        });
})

//Serve UI
auifiddle_server.use(express.static(__dirname+'/sandbox_ui/'));

//POST
auifiddle_server.post("/code", function(req, res){
	var saveData = {
		js: req.body.js,
		css: req.body.css,
		html: req.body.html
	};
	generateNewId(function(newId){
		saveCode(newId, saveData, res);
	});
});

auifiddle_server.put("/code/:hash", function(req, res){
	var saveData = {
		js: req.body.js,
		css: req.body.css,
		html: req.body.html
	};

	//find the latest revision
	request({
		url: 'http://' + CONFIG.host + ':' + CONFIG.dbport + '/' + CONFIG.db + '/' + req.params.hash, 
		method: "GET"},
		function(err, response, data){
			saveData._rev = JSON.parse(data)._rev;
			saveCode(req.params.hash, saveData, res);	
		}

);

	
});

auifiddle_server.listen(CONFIG.port);

function getCodeRevision(codehash, revision, res){
	var get_revisions_url = 'http://' + CONFIG.host + ':' + CONFIG.dbport + '/' + CONFIG.db + '/' + codehash + "?revs_info=true";	
	request(get_revisions_url, function(err, response, data){
		var revInfo = JSON.parse(data)._revs_info;
		getCode(codehash, revInfo[revInfo.length-revision].rev, res);
		
	});
}

function getCode(codehash, revision, res){
	var request_url = 'http://' + CONFIG.host + ':' + CONFIG.dbport + '/' + CONFIG.db + '/' + codehash;

	if(revision){
		request_url = request_url + "?rev=" + revision;
	}
        request(
        	{
        		url: request_url,
        		method: "GET"
        	}, 
        	function(err, response, data){
                var result = JSON.parse(data),
                        code = JSON.stringify({
                                js: result.js,
                                html: result.html,
                                css: result.css
                        });
                res.writeHead(200, {'Content-Type': "application/json"});
                
                res.write(code);
                res.end();
        });
}

function saveCode(codehash, code, res){

	var put_url = 'http://' + CONFIG.host + ':' + CONFIG.dbport + '/' + CONFIG.db + '/' + codehash;
	request(
        {
        	url: put_url,
        	method: "PUT",
        	body: JSON.stringify(code)
        }, 
        function(err, response, data){
        	var responseObj = JSON.parse(data);
        	var obj = {id: responseObj.id};
        	var rev = responseObj.rev.split("-")[0];

        	if(parseInt(rev) > 1){
        		obj.rev = rev;
        	}
            res.writeHead(200, {'Content-Type': "application/json"});
            res.write(JSON.stringify(obj));
            res.end();
    	}
    );

}

function generateNewId(callback){
	request("http://" + CONFIG.host + ":" + CONFIG.dbport + "/_uuids", function(err, response, data){
		var newID = JSON.parse(data).uuids[0];
		checkID(newID, function(exists, newID){
			if(!exists){
				if(typeof callback==="function"){
					callback(newID);
				}
			}
		});	
	});

}

function checkID(id, callback){
	request("http://" + CONFIG.host + ":" + CONFIG.dbport + "/" + CONFIG.db + "/" + id, function(err, response, data){
		if(typeof callback==="function"){
			callback(data.error=="not_found", id);
		}
	});	
}