package com.atlassian.aui.test.contacts.rest;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.aui.test.contacts.entities.Contact;
import com.atlassian.aui.test.util.CollectionReorderer;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 */
public class ContactStore
{


    public static List<ContactBean> getContacts(final ActiveObjects ao)
    {
        final Contact[] contactEntities = ao.find(Contact.class);
        Map<Integer, ContactBean> contacts = Maps.newTreeMap();
        for (Contact contact : contactEntities) // (2)
        {
            contacts.put(contact.getPos(), new ContactBean(contact.getId(), contact.getName(), contact.getGroup(), contact.getPhoneNumber()));
        }

        final ArrayList<ContactBean> contactBeans = Lists.newArrayList(contacts.values());
        Collections.reverse(contactBeans);
        return contactBeans;
    }

    public static Result<ContactBean> validateContact(ActiveObjects ao, final ContactBean contact)
    {
        final List<ContactBean> contacts = getContacts(ao);
        final Result.Builder<ContactBean> contactBuilder = new Result.Builder<ContactBean>();

        contactBuilder.setResult(contact);

        for (ContactBean currContact : contacts)
        {
            if (currContact.getName().equals(contact.getName()))
            {
                contactBuilder.addError("name", "You have another contact with the same name");
            }
        }

        if (contact.getNumber() != null)
        {
            try
            {
                Integer.parseInt(contact.getNumber(), 10);
            }
            catch (Exception e)
            {
                contactBuilder.addError("number", "Not a valid number");
            }
        }

        return contactBuilder.build();
    }

    public static ContactBean mergeBeans(ContactBean contact, ContactBean modifications)
    {
        if (modifications.getName() != null)
        {
            contact.setName(modifications.getName());
        }
        if (modifications.getGroup() != null)
        {
            contact.setGroup(modifications.getGroup());
        }
        if (modifications.getNumber() != null)
        {
            contact.setNumber(modifications.getNumber());
        }
        return contact;
    }

    private static ContactBean updateContact(final ActiveObjects ao, final ContactBean contactBean)
    {
        ao.executeInTransaction(new TransactionCallback<Void>() // (1)
        {
            public Void doInTransaction()
            {
                Contact contact = ao.get(Contact.class, contactBean.getId());
                if (contact != null)
                {
                    contact.setName(contactBean.getName());
                    contact.setGroup(contactBean.getGroup());
                    contact.setPhoneNumber(contactBean.getNumber());
                    contact.save();
                }
                return null;
            }
        });


        return contactBean;
    }

    public static void deleteContact(final ActiveObjects ao, final Long id)
    {
        ao.executeInTransaction(new TransactionCallback<Void>() // (1)
        {
            public Void doInTransaction()
            {
                Contact contact = ao.get(Contact.class, id);
                if (contact != null)
                {
                    ao.delete(contact);
                }
                return null;
            }
        });
    }

    public static Result<ContactBean> mergeModsIntoContact(ActiveObjects ao, ContactBean contact, final ContactBean modifications)
    {
        final Result<ContactBean> contactResult = validateContact(ao, modifications);

        if (contactResult.isValid())
        {
            contact = mergeBeans(contact, contactResult.getResult());
            updateContact(ao, contact);
            contactResult.setResult(contact);
        }

        return contactResult;
    }

    public static Result<ContactBean> createNewContactFromBean(final ActiveObjects ao, final ContactBean contactBean)
    {
        final Result<ContactBean> contactResult = validateContact(ao, contactBean);
        if (contactResult.isValid())
        {
            ao.executeInTransaction(new TransactionCallback<Contact>() // (1)
            {
                public Contact doInTransaction()
                {
                    final List<Contact> sortedContacts = getSortedContacts(ao);

                    Contact contact = ao.create(Contact.class);
                    contactBean.setId(contact.getId());
                    contact.setName(contactBean.getName());
                    contact.setGroup(contactBean.getGroup());
                    contact.setPhoneNumber(contactBean.getNumber());
                    contact.save();

                    sortedContacts.add(contact);

                    for (Contact sortedContact : sortedContacts)
                    {
                        sortedContact.setPos(sortedContacts.indexOf(sortedContact));
                        sortedContact.save();
                    }

                    return contact;
                }
            });
        }
        return contactResult;
    }

    private static List<Contact> getSortedContacts(final ActiveObjects ao)
    {
        Map<Integer, Contact> sortedContacts = Maps.newTreeMap();
        final Contact[] contactEntities = ao.find(Contact.class);
        for (Contact contact : contactEntities) // (2)
        {
            sortedContacts.put(contact.getPos(), contact);
        }
        return Lists.newArrayList(sortedContacts.values());
    }

    private static void performContactOrdering(final ActiveObjects ao, final long id, OrderingTransaction callback)
    {


        Contact contactToSort = null;
        final Contact[] contactEntities = ao.find(Contact.class);
        Map<Integer, Contact> sortedContacts = Maps.newTreeMap();
        for (Contact contact : contactEntities) // (2)
        {
            sortedContacts.put(contact.getPos(), contact);
            if (contact.getId() == id)
            {
                contactToSort = contact;
            }
        }

        List<Contact> sortedContactsList = Lists.newArrayList(sortedContacts.values());
        callback.doTransaction(sortedContactsList, contactToSort);

        for (Contact contact : sortedContactsList)
        {
            contact.setPos(sortedContactsList.indexOf(contact));
            contact.save();
        }
    }


    public static void increaseContactSequence(final ActiveObjects ao, final long id)
    {
        ao.executeInTransaction(new TransactionCallback<Void>() // (1)
        {
            public Void doInTransaction()
            {
                performContactOrdering(ao, id, new OrderingTransaction() // (1)
                {
                    public List<Contact> doTransaction(List<Contact> sortedContacts, Contact contactToSort)
                    {
                        final CollectionReorderer<Contact> collectionReorderer = new CollectionReorderer<Contact>();
                        collectionReorderer.increasePosition(sortedContacts, contactToSort);
                        return sortedContacts;
                    }
                });
                return null;
            }
        });
    }

    public static void decreaseContactSequence(final ActiveObjects ao, final long id)
    {
        ao.executeInTransaction(new TransactionCallback<Void>() // (1)
        {
            public Void doInTransaction()
            {

                performContactOrdering(ao, id, new OrderingTransaction() // (1)
                {
                    public List<Contact> doTransaction(List<Contact> sortedContacts, Contact contactToSort)
                    {
                        final CollectionReorderer<Contact> collectionReorderer = new CollectionReorderer<Contact>();
                        collectionReorderer.decreasePosition(sortedContacts, contactToSort);
                        return sortedContacts;
                    }
                });
                return null;
            }
        });
    }


    public static void moveToStartContactSequence(final ActiveObjects ao, final long id)
    {
        ao.executeInTransaction(new TransactionCallback<Void>() // (1)
        {
            public Void doInTransaction()
            {

                performContactOrdering(ao, id, new OrderingTransaction() // (1)
                {
                    public List<Contact> doTransaction(List<Contact> sortedContacts, Contact contactToSort)
                    {
                        final CollectionReorderer<Contact> collectionReorderer = new CollectionReorderer<Contact>();
                        collectionReorderer.moveToStart(sortedContacts, contactToSort);
                        return sortedContacts;
                    }
                });
                return null;
            }
        });
    }

    public static void moveToEndContactSequence(final ActiveObjects ao, final long id)
    {
        ao.executeInTransaction(new TransactionCallback<Void>() // (1)
        {
            public Void doInTransaction()
            {

                performContactOrdering(ao, id, new OrderingTransaction() // (1)
                {
                    public List<Contact> doTransaction(List<Contact> sortedContacts, Contact contactToSort)
                    {
                        final CollectionReorderer<Contact> collectionReorderer = new CollectionReorderer<Contact>();
                        collectionReorderer.moveToEnd(sortedContacts, contactToSort);
                        return sortedContacts;
                    }
                });
                return null;
            }
        });
    }

    public static void moveContactAfter(final ActiveObjects ao, final long id, final long afterId)
    {
        ao.executeInTransaction(new TransactionCallback<Void>() // (1)
        {
            public Void doInTransaction()
            {

                performContactOrdering(ao, id, new OrderingTransaction() // (1)
                {
                    public List<Contact> doTransaction(List<Contact> sortedContacts, Contact contactToSort)
                    {
                        final CollectionReorderer<Contact> collectionReorderer = new CollectionReorderer<Contact>();

                        final Contact afterContact = ao.get(Contact.class, afterId);

                        if (afterContact != null)
                        {
                            collectionReorderer.moveToPositionAfter(sortedContacts, contactToSort, afterContact);
                        }
                        else
                        {
                            collectionReorderer.moveToStart(sortedContacts, contactToSort);
                        }
                        return sortedContacts;
                    }
                });
                return null;
            }
        });
    }

    static void deleteAllContacts(final ActiveObjects ao)
    {
        final Contact[] contacts = ao.find(Contact.class);
        for(Contact contact : contacts)
        {
            ao.delete(contact);
        }
        ao.flushAll();
    }

    public static interface OrderingTransaction
    {
        public List<Contact> doTransaction(List<Contact> sortedContacts, Contact contactToSort);
    }


}
