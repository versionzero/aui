package it.com.atlassian.aui.javascript.pages;


import javax.inject.Inject;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.aui.component.restfultable.RestfulTable;


public class RestfulTablePage extends TestPage
{
    @Inject
    private PageBinder binder;

    @Override
    public String getUrl()
    {
        return "/plugins/servlet/ajstest/restfultable/";
    }

    public RestfulTable getTable()
    {
        return binder.bind(RestfulTable.class, "contacts-table");
    }

    /**
     * Gets the instance of RestfulTable with the addPosition option set to "bottom".
     * @return
     */
    public RestfulTable getTableAddPositionBottom()
    {
        return binder.bind(RestfulTable.class, "contacts-table-addPositionBottom");
    }
}
