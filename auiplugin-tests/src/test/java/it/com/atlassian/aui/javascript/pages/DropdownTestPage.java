package it.com.atlassian.aui.javascript.pages;

/**
 * @since 3.7
 */
public class DropdownTestPage extends TestPage
{
    public String getUrl()
    {
        return "/plugins/servlet/ajstest/test-pages/dropdown/dropdown-test.html";
    }
}
