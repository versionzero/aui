package it.com.atlassian.aui.javascript.pages;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.aui.component.AuiDatePicker;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * @since 3.7
 */
public class DatePickerTestPage implements Page
{
    @Inject
    PageBinder binder;

    @Override
    public String getUrl()
    {
        return "/plugins/servlet/ajstest/test-pages/experimental/date-picker/date-picker-test.html";
    }

    public AuiDatePicker getBasicDatePicker()
    {
        return binder.bind(AuiDatePicker.class, By.id("test-basic-always"));
    }

    public AuiDatePicker getDefaultDatePicker()
    {
        return binder.bind(AuiDatePicker.class, By.id("test-default-always"));
    }

    public AuiDatePicker getRangeDatePicker()
    {
        return binder.bind(AuiDatePicker.class, By.id("test-range-always"));
    }

    public AuiDatePicker getDifferentStartDayDatePicker()
    {
        return binder.bind(AuiDatePicker.class, By.id("test-start-always"));
    }

    public AuiDatePicker getCustomLanguageDatePicker()
    {
        return binder.bind(AuiDatePicker.class, By.id("test-lang-always"));
    }
}
